extends TileMap


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	for x in range(-50,50):
		for y in range(-50,50):
			self.set_cell(x,y,self.get_cell(x,y),randi()%2,randi()%2)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
