extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	for i in range(5):
		if Input.is_key_pressed(KEY_W) and collision(0,0):
			$Map.position[1] += 1
		if Input.is_key_pressed(KEY_S) and collision(2,0):
			$Map.position[1] -= 1
		if Input.is_key_pressed(KEY_A) and collision(3,0):
			$Map.position[0] += 1
		if Input.is_key_pressed(KEY_D) and collision(1,0):
			$Map.position[0] -= 1

func collision(direction,dist):
	var cell1 = null
	var cell2 = null
	if direction == 0:
		cell1 = $CollisionMap.get_cell(8+floor(-$Map.position[0]/64),4+ceil(((-$Map.position[1])-dist)/64))
		cell2 = $CollisionMap.get_cell(8+ceil(-$Map.position[0]/64),4+ceil(((-$Map.position[1])-dist)/64))
	elif direction == 1:
		cell1 = $CollisionMap.get_cell(9+floor(((-$Map.position[0])+dist)/64),5+floor(-$Map.position[1]/64))
		cell2 = $CollisionMap.get_cell(9+floor(((-$Map.position[0])+dist)/64),5+ceil(-$Map.position[1]/64))
	elif direction == 2:
		cell1 = $CollisionMap.get_cell(8+floor(-$Map.position[0]/64),6+floor(((-$Map.position[1])+dist)/64))
		cell2 = $CollisionMap.get_cell(8+ceil(-$Map.position[0]/64),6+floor(((-$Map.position[1])+dist)/64))
	elif direction == 3:
		cell1 = $CollisionMap.get_cell(7+ceil(((-$Map.position[0])-dist)/64),5+floor(-$Map.position[1]/64))
		cell2 = $CollisionMap.get_cell(7+ceil(((-$Map.position[0])-dist)/64),5+ceil(-$Map.position[1]/64))
		
	if cell1 == 0 and cell2 == 0:
		return true
