def turn(player,enemy):
    print("Player health: "+str(player.health))
    print("Enemy health: "+str(enemy.health))
    player.select_deck()
    player.select_card()
    result = minigame()
    damage = 10 if player.current_card.special else 2
    if result:
        if enemy.current_card.element_type == player.current_card.element_type:
            enemy.health += damage
        else:
            enemy.health -= damage
    else:
        if enemy.current_card.element_type == player.current_card.element_type:
            player.health += damage
        else:
            player.health -= damage
    if player.health <= 0:
        return 1
    if enemy.health <= 0:
        return 2
    return 0


def minigame():
    print("Win minigame?")
    return input()=="y"


class Card():
    def __init__(self, element_type, special: bool, name: str) -> None:
        self.name = name
        self.element_type = element_type
        self.special = special
    def __str__(self) -> str:
        return self.name+" "+("special" if self.special else "normal")+" "+str(self.element_type)


class CardHolder():
    def __init__(self,max_health: float, decks: list[list[Card]], current_deck: int=0, current_card: int=0) -> None:
        self.max_health = max_health
        self.health = max_health
        self.decks = decks
        self.current_deck_index = current_deck
        self.current_deck = decks[current_deck]
        self.current_card_index = current_card
        self.current_card = self.current_deck[current_card]

class Enemy(CardHolder):
    pass

class Player(CardHolder):
    def __init__(self,max_health: float, decks: list, current_deck: int=0, current_card: int=0) -> None:
        super().__init__(max_health, decks, current_deck, current_card)
    def select_card(self) -> Card:
        print("Which card?")
        for i,x in enumerate(self.current_deck):
            print(str(i)+": "+str(x))
        x = True
        while x:
            try:
                num = int(input())
                x = False
            except ValueError:
                print("Sorry, that's not a number.")
        self.current_card_index = num
        self.current_card = self.current_deck[self.current_card_index]
        return self.current_card
    def select_deck(self) -> list:
        print("Which deck?")
        for i,x in enumerate(self.decks):
            print(str(i)+": "+deck_to_str(x))
        x = True
        while x:
            try:
                num = int(input())
                x = False
            except ValueError:
                print("Sorry, that's not a number.")
        self.current_deck_index = num
        self.current_deck = self.decks[self.current_deck_index]
        return self.current_deck

def deck_to_str(deck):
    x = str()
    for card in deck:
        x += str(card)+"\n"
    return x[:-1]

player = Player(20,[[Card("water",True,"Penguin")],[Card("grass",False,"Sheep")]])
enemy = Enemy(10,[[Card("water",True,"Penguin")],[Card("grass",False,"Sheep")]])
deck = [Card("water",True,"Penguin")]
while 1:
    res = turn(player,enemy)
    if res != 0:
        break
if res == 2:
    print("Player wins :D")
else:
    print("Player lost :(")