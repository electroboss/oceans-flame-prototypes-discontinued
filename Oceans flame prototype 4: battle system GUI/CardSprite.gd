extends Sprite


func set_card(name, image, element, type, description):
	$Name.text = name
	$Image.texture = image
	$Element.text = element
	$Type.text = "special" if type else "normal"
	$Description.text = description
