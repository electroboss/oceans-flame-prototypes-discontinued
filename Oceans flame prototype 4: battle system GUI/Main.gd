extends Node2D

var current_player_card = 0
var current_enemy_card = 0

# Player

func _ready():
	current_player_card = get_player_card()
	current_enemy_card = get_enemy_card()
	update_cards()
	
	
	if CardsGlobals.minigame:
		var damage = 5
		if CardsGlobals.get_card_data(current_player_card)[3]:
			damage = damage + 5
		if CardsGlobals.get_card_data(current_enemy_card)[3]:
			damage = damage + 5
		if CardsGlobals.win:
			if CardsGlobals.get_card_data(current_player_card)[2] == CardsGlobals.get_card_data(current_enemy_card)[2]:
				CardsGlobals.enemy_health += damage
			else:
				CardsGlobals.enemy_health -= damage
		else:
			if CardsGlobals.get_card_data(current_player_card)[2] == CardsGlobals.get_card_data(current_enemy_card)[2]:
				CardsGlobals.player_health += damage
			else:
				CardsGlobals.player_health -= damage
	$FG/Label.text = "Player health: " + str(CardsGlobals.player_health) + "\nEnemy health: " + str(CardsGlobals.enemy_health)
	CardsGlobals.minigame = false
	if CardsGlobals.player_health <= 0:
		get_tree().change_scene("res://Lose.tscn")
	if CardsGlobals.enemy_health <= 0:
		get_tree().change_scene("res://Win.tscn")
	
func update_cards():
	update_card($FG/PlayerCard,current_player_card)
	update_card($FG/EnemyCard,current_enemy_card)
func update_card(card, id):
	var card_data = CardsGlobals.get_card_data(id)
	card.set_card(
		card_data[0],
		card_data[1],
		card_data[2],
		card_data[3],
		card_data[4]
	)
func get_player_card():
	return 0

func get_enemy_card():
	return 1
