extends Node


var player_health = 100
var enemy_health = 100

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

var cards = [
#	Name		Image									Element		Type	Description			Attack power
	["Penguin",	load("res://cards/penguin/image.tres"),	"water",	false,	"*penguin nosies*",	20],
	["Rabbit",	load("res://cards/rabbit/image.tres"),	"grass",	true,	"*rabbit noises*",	15]
]
var win = false
var minigame = false

func get_card_data(id):
	return cards[id]
