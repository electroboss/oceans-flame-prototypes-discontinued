extends Node2D

var state = 0		# State of the program.
var notes = [false,false,false,false]
var piano_sequence = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if state == 0:
		if Input.is_key_pressed(KEY_W):
			$TileMap.position[1] += 5
		if Input.is_key_pressed(KEY_S):
			$TileMap.position[1] -= 5
		if Input.is_key_pressed(KEY_A):
			$TileMap.position[0] += 5
		if Input.is_key_pressed(KEY_D):
			$TileMap.position[0] -= 5
		if Input.is_key_pressed(KEY_SPACE):
			space()
		if Input.is_key_pressed(KEY_E):
			change_state(2)
	elif state == 1: # piano
		if Input.is_key_pressed(KEY_ESCAPE):
			change_state(0)
	elif state == 2: # journal/inventory
		if Input.is_key_pressed(KEY_ESCAPE):
			change_state(0)
		
func space():
	var tilemap_coords = [int(8-($TileMap.position[0]/64)),int(5-($TileMap.position[1]/64))]
	if $TileMap.get_cell(tilemap_coords[0],tilemap_coords[1])==1:
		change_state(1)
	elif $TileMap.get_cell(tilemap_coords[0],tilemap_coords[1])==2:
		add_note(get_note(tilemap_coords[0],tilemap_coords[1]))
	
func change_state(s):
	state = s
	$Piano.visible = false
	$Inventory.visible = false
	if s == 0:
		pass
	elif s == 1:
		$Piano.visible = true
	elif s == 2:
		$Inventory.visible = true
		$Inventory/RichTextLabel.text = "Journal:\n"
		if notes[0]:
			$Inventory/RichTextLabel.text += "Note 1: 1\n"
		if notes[1]:
			$Inventory/RichTextLabel.text += "Note 2: 5\n"
		if notes[2]:
			$Inventory/RichTextLabel.text += "Note 3: 2\n"
		if notes[3]:
			$Inventory/RichTextLabel.text += "Note 4: 3"

func get_note(x,y):
	if x==3 and y==2:
		return 0
	elif x==12 and y==2:
		return 1
	elif x==3 and y==7:
		return 2
	elif x==12 and y==7:
		return 3
	else:
		assert(false)

func add_note(note):
	notes[note] = true
	
func piano_sequence_completed():
	$TileMap.set_cell(7,0,4)
	$TileMap.set_cell(8,0,4,true)

func _on_Key_button_down(key):
	if key == 1:
		piano_sequence = 1
	elif key == 5 && piano_sequence == 1:
		piano_sequence = 2
	elif key == 2 && piano_sequence == 2:
		piano_sequence = 3
	elif key == 3 && piano_sequence == 3:
		piano_sequence_completed()
	else:
		piano_sequence = 0
